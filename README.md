# Startracker


Welcome to `startracker` project.
This set of tools allows to extract the pointing offset of the IACT telescope using the stars in the field of view.

You can find quick start guide together with detailed documentation on the [startacker  website][documentation_website].

If you would like to contribute to the project please start with reading the [Contributor's Guide][contributors].

Enjoy!

[contributors]:https://ctaunigesw-startracker.web.cern.ch/contributing.html
[documentation_website]:https://ctaunigesw-startracker.web.cern.ch
