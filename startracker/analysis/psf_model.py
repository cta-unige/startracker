'''
Module representing point spread function model
'''
from abc import ABC, abstractmethod
from scipy.stats import laplace, laplace_asymmetric
import logging
import numpy as np

class PSFModel(ABC):
    '''
    Interface for PSF model. Abstract class
    '''
    def __init__(self, logger=None):
        if logger is not None:
            self.log = logger
        else:
            self.log = logging.getLogger('startracker.psf_model')
            self.log.setLevel(logging.ERROR)

    @abstractmethod
    def pdf(self, *args):
        pass

    @abstractmethod
    def update_model_parameters(self, *args):
        pass

class ComaModel(PSFModel):
    '''
    PSF model, describing pure coma aberrations PSF effect
    '''
    def __init__(self, model_parameters, **kwargs):
        super().__init__(**kwargs)
        self.asymmetry_params = model_parameters['asymmetry']
        self.radial_scale_params = model_parameters['radial_scale']
        self.az_scale_params = model_parameters['az_scale']
        self.radial_component = laplace_asymmetric
        self.azimuthal_component = laplace

    def k_func(self, x):
        '''
        PSF radial component asymmetry dependency as a function of distance to camera center

        :param float x: distance to camera center [m]
        '''
        return 1 - self.asymmetry_params[0] * np.tanh(self.asymmetry_params[1]*x) - self.asymmetry_params[2] * x

    def sr_func(self, x):
        '''
        PSF radial component scale dependency as a function of distance to camera center

        :param float x: distance to camera center [m]
        '''
        return self.radial_scale_params[0] - self.radial_scale_params[1]*x +\
               self.radial_scale_params[2]*x**2 - self.radial_scale_params[3]*x**3

    def sf_func(self, x):
        '''
        PSF azimuthal component scale dependency as a function of distance to camera center

        :param float x: distance to camera center [m]
        '''
        return self.az_scale_params[0] * np.exp(-self.az_scale_params[1]*x) +\
               self.az_scale_params[2]/(self.az_scale_params[2] + x)

    def pdf(self, r, f):
        '''
        Probability distribution function for a star photon to arrive at (r,φ) coordinate in camera frame

        :param float r: R coordinate in polar frame [m]
        :param float φ: Φ coordinate in polar frame [rad]

        :return: Value of the probability distribution function in a given point
        '''
        return self.radial_component.pdf(r, *self.radial_pdf_params) *\
               self.azimuthal_component.pdf(f, *self.azimuthal_pdf_params)

    def update_model_parameters(self, r, f):
        '''
        Update model parameters as a function of the offset w.r.t. camera center

        :param float r: Distance to the camera center [m]
        :param float f: Polar angle [rad]
        '''
        self.log.debug('Update PSF model parameters')
        k = self.k_func(r)
        sr = self.sr_func(r)
        sf = self.sf_func(r)
        self.radial_pdf_params = (k, r, sr)
        self.azimuthal_pdf_params = (f, sf)
