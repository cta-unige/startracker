import numpy as np

def cart2pol(x, y):
    '''
    Convert cartesian coordinates to polar

    :param float x: X coordinate [m]
    :param float y: Y coordinate [m]

    :return: Tuple (r, φ)[m, rad]
    '''
    rho = np.sqrt(x**2 + y**2)
    phi = np.arctan2(y, x)
    return(rho, phi)

def pol2cart(rho, phi):
    '''
    Convert polar coordinates to cartesian

    :param float rho: R coordinate
    :param float phi: ¢ coordinate [rad]

    :return: Tuple (x,y)[m, m]
    '''
    x = rho * np.cos(phi)
    y = rho * np.sin(phi)
    return(x, y)
