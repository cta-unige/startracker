**********************
Detailed documentation
**********************

.. toctree::
   :maxdepth: 2

   physics
   usage
   api
