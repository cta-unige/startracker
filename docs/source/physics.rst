Physics behind the the StarTracker
##################################

Single star imaging and reconstruction
--------------------------------------

.. image:: _static/LSTGeneralMeetingStarTracking/Slide4.png

.. image:: _static/LSTGeneralMeetingStarTracking/Slide5.png

..
    .. image:: _static/LSTGeneralMeetingStarTracking/Slide6.png


Coma aberrations, PSF and resolution
--------------------------------------

.. image:: _static/LSTGeneralMeetingStarTracking/Slide7.png

The most up to date description of the PSF treatment can be found in the paper draft here:
https://www.overleaf.com/read/bntxgsszrrxr

There one can find as well details on the single star position determination

Star trajectory, its fitting and pointing extraction
----------------------------------------------------

.. image:: _static/LSTGeneralMeetingStarTracking/Slide10.png

.. image:: _static/LSTGeneralMeetingStarTracking/Slide11.png

.. image:: _static/LSTGeneralMeetingStarTracking/Slide12.png

.. image:: _static/LSTGeneralMeetingStarTracking/Slide13.png

