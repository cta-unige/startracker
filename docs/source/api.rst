API documentation
#################

Image Production
================
.. currentmodule:: startracker.tools.image_producer 

.. autoclass:: ImageProducer
   :show-inheritance:

:mod:`startracker.analysis.image`
---------------------------------
.. automodule:: startracker.analysis.image
    :members:
    :show-inheritance:

PSF model
=========
:mod:`startracker.analysis.psf_model`
-------------------------------------
.. currentmodule:: startracker.analysis.psf_model

.. automodule:: startracker.analysis.psf_model
    :members:
    :show-inheritance:

Utils
=====
.. automodule:: startracker.analysis.utils
    :members:
    :show-inheritance:

Trajectory fitting
==================
.. currentmodule:: startracker.tools.star_fitter

.. autoclass:: FitRunner
   :show-inheritance:

:mod:`startracker.analysis.fitter`
----------------------------------
.. automodule:: startracker.analysis.fitter
    :members:

Helper scripts
==============

process_subrun
--------------
.. argparse::
    :module: startracker.scripts.process_subrun
    :func: get_parser
    :prog: process_subrun
